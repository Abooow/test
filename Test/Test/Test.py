import random, os

#                         |<--------------------------------------------------------------------------------------------------<|
#             |<----------|<-----------------------------------------------------------------------<|                          |
#             |           |                                                                         |                          |
#             |           |                                                  |<---------<|          |                          |
# states = -->L> "reset" >L> "newRound" -> "bet" -> "getFirstCards" -> "checkCards" -> "play"    "lose" <- "compareCards" -> "win"
#                                                                            L>----------|----------^             ^
#                                                                            L>----------|>----"dealerPlay"-------|

cardSet = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
numOfDecks = 1
cards = []

playerCards = []
dealerCards  = []
count = 0
cash = 1000
lastBet = 0
state = "reset"


# ------------------------------ Functions
def get_total_sum(cards):
    sum = 0
    extra = 0
    for c in cards:
        if c == 'A':
            extra += 10
        sum += card_to_value(c) 

    if extra != 0 and not (sum + extra > 21):
        return f"{sum}/{sum+extra}"
    else:
        return sum


def get_sum(cards):
    return int(str(get_total_sum(cards)).split('/')[-1])


def check_cards(cards):
    sum = get_sum(cards)
    if len(cards) == 2 and sum == 21:  # BLACKJACK
        return "BLACKJACK"
    elif sum > 21: # BUSTED
        return "BUSTED"
    else:
        return sum


def card_to_value(card):
    try:
        return int(card)
    except:
        if card == 'A':
            return 1
        else:
            return 10


def get_card():
    global count
    card = cards.pop(random.randint(0, len(cards) - 1))
    try:
        value = int(card)

        if value <= 6:
            count += 1
        elif value == 10:
            count -= 1
    except:
        count -= 1
    return card


def shuffle_deck():
    global cards
    cards = cardSet[:] * 4 * numOfDecks


# ------------------------------- Gameloop -------------------------------
while True:
    extraPause = False
    # ----------------------------------------------------- Reset state
    if state == "reset":
        cash = 1000
        lastBet = 0
        shuffle_deck()
        state = "newRound"
        continue
    # ----------------------------------------------------- NewRound state
    elif state == "newRound":
        playerCards = []
        dealerCards  = []
        state = "bet"
        continue
    # ----------------------------------------------------- Bet state
    elif state == "bet":
        if cash == 0:
            state = "reset"
            continue

        print(f"You have ${cash}")
        print("How much would you like to bet?", end='')
        if lastBet != 0:
            print(f" (last bet: ${lastBet})", end='')
        bet = input("\n\nBet $")

        if bet != "":
            try:
                bet = int(bet)

                if bet > cash:
                    print("You don't have enough cash!")
                    extraPause = True
                elif bet < 1:
                    print("Enter a number higher than 0!")
                    extraPause = True
                else:
                    lastBet = bet
                    cash -= bet
                    state = "getFirstCards"
            except:
                print("\nInvalid input")
                extraPause = True
        elif lastBet > 0:
            bet = lastBet
            cash -= bet
            print("From last bet: $" + str(bet))
            state = "getFirstCards"
            extraPause = True
    # ----------------------------------------------------- GetFirstCards state
    elif state == "getFirstCards":
        if len(cards) <= 52 * numOfDecks * 0.5:
            print("Shuffling cards...")
            shuffle_deck()

        for i in range(2):
            dealerCards.append(get_card())
            playerCards.append(get_card())

        state = "checkCards"
        continue
    # ----------------------------------------------------- CheckCards state
    elif state == "checkCards":
        reslut = check_cards(playerCards)

        if reslut == "BLACKJACK": 
            state = "compareCards"
            continue
        elif reslut == "BUSTED":
            state = "lose"
            continue
        elif check_cards(dealerCards) == "BLACKJACK":
            state = "compareCards"
            continue

        state = "play"
        continue
    # ----------------------------------------------------- Play state
    elif state == "play":
        print(f"You have ${cash}, bet: ${bet}")
        print()
        print(f"Cards remaning: {len(cards)}")
        print(f"Count: {count}")
        print(f"  Dealer cards: ({get_total_sum(dealerCards[0])})\t{dealerCards[0]}, ?")
        print(f"  Player cards: ({get_total_sum(playerCards)})\t" + ', '.join(playerCards))
        print()
        print("1) Hit")
        print("2) Stand")
        choise = input("\nChoice >")

        if choise == "1":
            playerCards.append(get_card())
            state = "checkCards"
        elif choise == "2":
            state = "dealerPlay"
    # ----------------------------------------------------- DealerPlay state
    elif state == "dealerPlay":
        state = "compareCards"

        while True:
            # get card value
            dealerResult = check_cards(dealerCards)

            # check
            if dealerResult == "BUSTED": # busted
                state = "win"
                break
            elif dealerResult < 17:      # get card
                dealerCards.append(get_card());
            else:                        # stop
                break

        continue
    # ----------------------------------------------------- CompareCards state
    elif state == "compareCards":
        dealerResult = check_cards(dealerCards)
        playerResult = check_cards(playerCards)

        if dealerResult == playerResult:
            state = "win"
        elif playerResult == "BLACKJACK":
            state = "win"
        elif dealerResult == "BLACKJACK":
            state = "lose"
        elif playerResult > dealerResult:
            state = "win"
        else:                                                                                                                                                                                                                           
            state = "lose"

        continue
    # ----------------------------------------------------- Win state
    elif state == "win":
        cashWon = bet * 2
        dealerBust = ""
        causeOfWinning = ""
        if check_cards(playerCards) == "BLACKJACK":
            causeOfWinning = " (BLACKJACK)"
            cashWon = int(bet * 2.5)
        if check_cards(dealerCards) == "BUSTED":
            dealerBust = " (BUSTED)"

        print(f"You have ${cash}, bet: ${bet}")
        print()
        print(f"Cards remaning: {len(cards)}")
        print(f"Count: {count}")
        print(f"  Dealer cards: ({get_total_sum(dealerCards)})\t" + ', '.join(dealerCards) + dealerBust)
        print(f"  Player cards: ({get_total_sum(playerCards)})\t" + ', '.join(playerCards) + causeOfWinning)
        print()
        print(f"You won +${cashWon}")
        print("Press a key to play again..")

        cash += cashWon
        extraPause = True
        state = "newRound"
    # ----------------------------------------------------- Lose state
    elif state == "lose":
        causeOfLosing = ""
        dealerBlackJack = ""
        if check_cards(playerCards) == "BUSTED":
            causeOfLosing = " (BUSTED)"
        if check_cards(dealerCards) == "BLACKJACK":
            dealerBlackJack = " (BLACKJACK)"

        print(f"You have ${cash}, bet: ${bet}")
        print()
        print(f"Cards remaning: {len(cards)}")
        print(f"Count: {count}")
        print(f"  Dealer cards: ({get_total_sum(dealerCards)})\t" + ', '.join(dealerCards) + dealerBlackJack)
        print(f"  Player cards: ({get_total_sum(playerCards)})\t" + ', '.join(playerCards) + causeOfLosing)
        print()
        print(f"You lost -${bet}")
        print("Press a key to play again..")

        extraPause = True
        state = "newRound"
    # ----------------------------

    # --------------- Clear screen
    if extraPause:
        input()
    os.system('cls')